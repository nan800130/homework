DROP TABLE IF EXISTS currency_info;
CREATE TABLE currency_info (
	code varchar(3) NOT NULL,
	name varchar(10) NOT NULL,
	create_time timestamp NULL,
	update_time timestamp NULL,
	CONSTRAINT currency_info_pk PRIMARY KEY (code)
);

COMMENT ON COLUMN currency_info.code IS '幣別代碼';
COMMENT ON COLUMN currency_info.name IS '貨幣名稱';
COMMENT ON COLUMN currency_info.create_time IS '建立時間';
COMMENT ON COLUMN currency_info.update_time IS '更新時間';

INSERT INTO currency_info(code, name, create_time, update_time) VALUES ('USD', '美金', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO currency_info(code, name, create_time, update_time) VALUES ('GBP', '英鎊', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO currency_info(code, name, create_time, update_time) VALUES ('EUR', '歐元', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);