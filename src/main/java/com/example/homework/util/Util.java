package com.example.homework.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Util {
    public static LocalDateTime parseISODateTime(String str) {

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME;

        LocalDateTime localDateTime = LocalDateTime.parse(str, dateTimeFormatter);

        return localDateTime;
    }
}
