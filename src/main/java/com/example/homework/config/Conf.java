package com.example.homework.config;

public class Conf {
    public static final String EMPTY = "";
    public static final String SPACE = " ";
    public static final String COMMA = ",";
    public static final String LEFT_PARENTHESES = "(";
    public static final String RIGHT_PARENTHESES = ")";

    public static final String DATETIME_FORMAT_PATTERN = "yyyy/MM/dd HH:mm:ss";
}
