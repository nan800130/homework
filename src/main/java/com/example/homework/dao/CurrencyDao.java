package com.example.homework.dao;


import com.example.homework.model.Currency;
import com.example.homework.repository.CurrencyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
@Service
public class CurrencyDao{
    @Autowired
    private CurrencyRepository currencyRepository;

    public Currency createOrUpdateCurrency(Currency currency) {
        return currencyRepository.save(currency);
    }

    public List<Currency> findAllCurrency() {
        return currencyRepository.findAll();
    }

    public Optional<Currency> findCurrencyByCode(String code) {
        return currencyRepository.findByCode(code);
    }

    public void deleteCurrencyByCode(String code) {
        currencyRepository.deleteByCode(code);
    }
}
