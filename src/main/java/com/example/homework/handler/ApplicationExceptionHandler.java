package com.example.homework.handler;

import com.example.homework.config.Conf;
import com.example.homework.dto.Response;
import com.example.homework.enumeration.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class ApplicationExceptionHandler {
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Object methodArgumentNotValidExceptionHandler(final MethodArgumentNotValidException e){
        StringBuilder sb = new StringBuilder();
        sb.append(Result.PARAMETER_VALIDATION_FAILURE.getDesc())
                .append(Conf.LEFT_PARENTHESES);

        e.getBindingResult().getFieldErrors().forEach(
                fieldError -> sb.append(String.format("%s %s", fieldError.getField(), fieldError.getDefaultMessage()))
                        .append(Conf.COMMA).append(Conf.SPACE));
        sb.replace(sb.lastIndexOf(Conf.COMMA), sb.length(), Conf.EMPTY);

        sb.append(Conf.RIGHT_PARENTHESES);

        String message = sb.toString();
        sb.delete(0, sb.length());

        Response response = new Response();
        response.setCode(Result.PARAMETER_VALIDATION_FAILURE.getCode());
        response.setMessage(message);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }

    @ExceptionHandler(RuntimeException.class)
    public Object exceptionHandler(final Exception e) {
        Response response = new Response();
        response.setCode(Result.SYSTEM_ERROR.getCode());
        response.setMessage(Result.SYSTEM_ERROR.getDesc());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
    }
}
