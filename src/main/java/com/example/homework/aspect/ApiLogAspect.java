package com.example.homework.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;

@Slf4j
@Aspect
@Component
public class ApiLogAspect {
    @Pointcut("execution(* com.example.homework.controller..*(..))")
    public void pointcut() {
    }

    @Around("pointcut()")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Instant startTime = Instant.now();
        Object obj = proceedingJoinPoint.proceed();
        Instant endTime = Instant.now();

        log.info("API Execute - Controller: {}, Function: {}, Parameter: {}, Result:{}, SpendTime: {} ms", proceedingJoinPoint.getTarget().getClass().getName(), proceedingJoinPoint.getSignature().getName(), proceedingJoinPoint.getArgs(), obj, Duration.between(startTime, endTime).toMillis());
        return obj;
    }
}
