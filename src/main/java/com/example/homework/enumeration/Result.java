package com.example.homework.enumeration;

import lombok.Getter;

@Getter
public enum Result {
    RESULT_SUCCESS("0000", "SUCCESS"), //Success Code
    NO_DATA_FOUND("0001", "查無資料"),  //NOT FOUND Code
    DATA_DUPLICATED("0002", "資料重複"), //Duplicate Code
    PARAMETER_VALIDATION_FAILURE("0003", "參數驗證錯誤"), //Validation Failure Code
    SYSTEM_ERROR("9999", "系統執行錯誤"),; //System Error Code

    private String code;
    private String desc;

    Result(String code, String desc){
        this.code = code;
        this.desc = desc;
    }
}
