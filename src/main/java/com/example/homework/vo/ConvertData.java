package com.example.homework.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
public class ConvertData {
    @JsonProperty("updateTime")
    private String updateTime;
    @JsonProperty("bpi")
    private Map<String, ConvertBpiItem> bpi;
}
