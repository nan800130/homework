package com.example.homework.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ConvertBpiItem extends BpiItem {
    @JsonProperty("chineseName")
    private String chineseName;
}
