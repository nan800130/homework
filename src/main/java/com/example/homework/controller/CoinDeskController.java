package com.example.homework.controller;

import com.example.homework.dto.Response;
import com.example.homework.service.CoinDeskService;
import com.example.homework.vo.CoinDeskData;
import com.example.homework.vo.ConvertData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// CoinDesk
@Slf4j
@RestController
@RequestMapping(path = "/coindesk")
public class CoinDeskController {
    @Autowired
    private CoinDeskService coinDeskService;

    @GetMapping(value = "")
    public ResponseEntity<Response<CoinDeskData>> getData() throws Exception {
        Response<CoinDeskData> response = new Response<CoinDeskData>();

        CoinDeskData coinDeskData = coinDeskService.getOrgData();

        //prepare response data
        response.setData(coinDeskData);

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping(value = "/convert")
    public ResponseEntity<Response<ConvertData>> convert() throws Exception {
        Response<ConvertData> response = new Response<ConvertData>();

        ConvertData convertData = coinDeskService.getConvertData();

        //prepare response data
        response.setData(convertData);

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
