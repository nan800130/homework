package com.example.homework.controller;


import com.example.homework.dto.currency.*;
import com.example.homework.model.Currency;
import com.example.homework.service.impl.CurrencyServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@Slf4j
@RestController
@RequestMapping(path = "/currency")
public class CurrencyController {
    @Autowired
    private CurrencyServiceImpl currencyService;

    //insert new currency
    @PostMapping(value = "")
    public ResponseEntity<AddCurrencyResp> insert(@Validated @RequestBody AddCurrencyReq addCurrencyReq) throws Exception {

        return ResponseEntity.status(HttpStatus.CREATED).body(currencyService.insert(addCurrencyReq));
    }

    //find all currencies data
    @GetMapping(value = "")
    public ResponseEntity<List<Currency>> getCurrencies() throws Exception {

        return ResponseEntity.status(HttpStatus.OK).body(currencyService.findAllCurrency());
    }

    //find certain currency data
    @GetMapping(value = "/{code}")
    public ResponseEntity<Optional<Currency>> get(@PathVariable String code) throws Exception {

        return ResponseEntity.status(HttpStatus.OK).body(currencyService.findCurrencyByCode(code));
    }

    // modify currency data
    @PatchMapping(value="/{code}")
    public ResponseEntity<Object> update(@PathVariable String code, @Validated @RequestBody ModifyCurrencyReq modifyCurrencyReq) throws Exception {

        return ResponseEntity.status(HttpStatus.OK).body(currencyService.createOrUpdateCurrency(code, modifyCurrencyReq));
    }

    //delete currency data with code
    @DeleteMapping(value="/{code}")
    public ResponseEntity<Void> delete(@Validated @PathVariable String code) throws Exception {

        currencyService.deleteCurrencyByCode(code);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}

