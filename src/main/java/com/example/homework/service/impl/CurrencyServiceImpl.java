package com.example.homework.service.impl;

import com.example.homework.dao.CurrencyDao;
import com.example.homework.dto.currency.*;
import com.example.homework.model.Currency;
import com.example.homework.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author shawn
 * @date 2022/12/29
 **/
@Service
public class CurrencyServiceImpl implements CurrencyService {

    @Autowired
    private CurrencyDao currencyDao;

    @Override
    public AddCurrencyResp insert(AddCurrencyReq addCurrencyReq) throws Exception {
        Optional<Currency> orgCurrency = currencyDao.findCurrencyByCode(addCurrencyReq.getCode());

        if(orgCurrency.isPresent()){
            throw new Exception();
        }

        //new an empty currency instance
        Currency newCurrency = new Currency();

        //setting the code and name properties from request body AddCurrencyReq
        newCurrency.setCode(addCurrencyReq.getCode().toUpperCase());
        newCurrency.setName(addCurrencyReq.getName());

        //persist the instance into database
        Currency currency = currencyDao.createOrUpdateCurrency(newCurrency);

        //prepare response data
        AddCurrencyResp addCurrencyResp = new AddCurrencyResp();
        addCurrencyResp.setCurrency(currency);

        return addCurrencyResp;
    }

    @Override
    public Currency createOrUpdateCurrency(String code, ModifyCurrencyReq modifyCurrencyReq) {

        //fetch certain currency data with code
        Optional<Currency> currency = currencyDao.findCurrencyByCode(code.toUpperCase());

        if (currency.isPresent()) {
            //update the information of currency
            currency.get().setName(modifyCurrencyReq.getName());

            //persist currency data into database
            Currency resultCurrency = currencyDao.createOrUpdateCurrency(currency.get());

            //prepare response data
            ModifyCurrencyResp modifyCurrencyResp = new ModifyCurrencyResp();
            modifyCurrencyResp.setCurrency(resultCurrency);
            }
        return null;
    }


    @Override
    public List<Currency> findAllCurrency() {

        //query all currencies into a list
        List<Currency> currencies = currencyDao.findAllCurrency();

        //prepare response data
        GetCurrencyListResp getCurrencyListResp = new GetCurrencyListResp();
        getCurrencyListResp.setCurrencyList(currencies);

        return currencies;
    }

    @Override
    public Optional<Currency> findCurrencyByCode(String code) {

        //query certain currency data with code
        Optional<Currency> currency = currencyDao.findCurrencyByCode(code.toUpperCase());

        if (currency.isPresent()) {

            //prepare response data
            GetCurrencyResp getCurrencyResp = new GetCurrencyResp();
            getCurrencyResp.setCurrency(currency.get());

        }else{
            return Optional.empty();
        }

        return currency;
    }

    @Override
    public void deleteCurrencyByCode(String code) {

        Optional<Currency> currency = currencyDao.findCurrencyByCode(code.toUpperCase());

        //check specified code of currency is exist or not
        if (!currency.isPresent()){
//            return Optional.empty();
        }else {
            //if desired currency exists, then delete currency data
            currencyDao.deleteCurrencyByCode(code.toUpperCase());
        }

    }
}

