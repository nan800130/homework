package com.example.homework.service.impl;

import com.example.homework.config.Conf;
import com.example.homework.model.Currency;
import com.example.homework.service.CoinDeskService;
import com.example.homework.service.CurrencyService;
import com.example.homework.util.Util;
import com.example.homework.vo.BpiItem;
import com.example.homework.vo.CoinDeskData;
import com.example.homework.vo.ConvertBpiItem;
import com.example.homework.vo.ConvertData;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class CoinDeskServiceImpl implements CoinDeskService {
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CurrencyService currencyService;

    @Value("${coindesk.url}")
    private String coinDeskUrl;

    @Override
    public CoinDeskData getOrgData() throws Exception {
        CoinDeskData coinDeskData = null;
        try {
            RestTemplate restTemplate = new RestTemplate();
            //get data as String format by calling coindesk API
            String data = restTemplate.getForObject(coinDeskUrl, String.class);

            //parse the String into a JSON format
            JsonNode rootNode = objectMapper.readTree(data);

            //transform JSON format into coinDesk Data
            coinDeskData = objectMapper.treeToValue(rootNode, CoinDeskData.class);
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
        return coinDeskData;
    }

    @Override
    public ConvertData getConvertData() throws Exception {
        ConvertData convertData = null;

        try {
            RestTemplate restTemplate = new RestTemplate();
            String data = restTemplate.getForObject(coinDeskUrl, String.class);
            JsonNode rootNode = objectMapper.readTree(data);
            CoinDeskData coinDeskData = objectMapper.treeToValue(rootNode, CoinDeskData.class);

            convertData = new ConvertData();

            // setting update time
            DateTimeFormatter dataTimeFormatter = DateTimeFormatter.ofPattern(Conf.DATETIME_FORMAT_PATTERN);
            convertData.setUpdateTime(Util.parseISODateTime(coinDeskData.getTime().getUpdatedISO()).format(dataTimeFormatter));

            // fetch all currency data
            List<Currency> currencies = currencyService.findAllCurrency();

            // setting convert bpiItem info
            Map<String, ConvertBpiItem> convertBpi = new HashMap<String, ConvertBpiItem>();
            Map<String, BpiItem> bpi = coinDeskData.getBpi();

            for(Map.Entry<String, BpiItem> entry : bpi.entrySet()) {

                // 複製匯率資料 (copy exchange rate)
                ConvertBpiItem convertBpiItem = new ConvertBpiItem();
                BeanUtils.copyProperties(entry.getValue(), convertBpiItem);

                // fetch currency name in chinese from database
                Currency currency = currencies.stream().filter(item -> item.getCode().equalsIgnoreCase(entry.getKey())).findAny().orElse(null);
                if(currency != null){
                    convertBpiItem.setChineseName(currency.getName());
                }
                convertBpi.put(entry.getKey(), convertBpiItem);
            }
            convertData.setBpi(convertBpi);
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
        return convertData;
    }
}
