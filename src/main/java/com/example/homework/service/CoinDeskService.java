package com.example.homework.service;

import com.example.homework.vo.CoinDeskData;
import com.example.homework.vo.ConvertData;

public interface CoinDeskService {
    CoinDeskData getOrgData() throws Exception;

    ConvertData getConvertData() throws Exception;
}
