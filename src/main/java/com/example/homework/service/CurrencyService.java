package com.example.homework.service;

import com.example.homework.dto.currency.AddCurrencyReq;
import com.example.homework.dto.currency.AddCurrencyResp;
import com.example.homework.dto.currency.ModifyCurrencyReq;
import com.example.homework.model.Currency;

import java.util.List;
import java.util.Optional;

public interface CurrencyService {

    AddCurrencyResp insert(AddCurrencyReq addCurrencyReq) throws Exception;


    Currency createOrUpdateCurrency(String code, ModifyCurrencyReq modifyCurrencyReq);

    // query all currencies data
    List<Currency> findAllCurrency();

    // query certain currency with code
    Optional<Currency> findCurrencyByCode(String code);

    // delete certain currency data with code
    void deleteCurrencyByCode(String code);
}
