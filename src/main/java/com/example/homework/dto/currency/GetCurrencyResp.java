package com.example.homework.dto.currency;

import com.example.homework.model.Currency;
import lombok.Data;

// Get Certain Currency Response
@Data
public class GetCurrencyResp {
    //currency info
    private Currency currency;
}
