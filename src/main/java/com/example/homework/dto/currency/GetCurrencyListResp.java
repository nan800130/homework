package com.example.homework.dto.currency;

import com.example.homework.model.Currency;
import lombok.Data;

import java.util.List;

// Get Currency List Response
@Data
public class GetCurrencyListResp {
    // currency list
    private List<Currency> currencyList;
}
