package com.example.homework.dto.currency;

import com.example.homework.model.Currency;
import lombok.Data;

//Modify Currency Response
@Data
public class ModifyCurrencyResp {
    //currency info
    private Currency currency;
}
