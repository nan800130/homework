package com.example.homework.dto.currency;

import com.example.homework.model.Currency;
import lombok.Data;

// Add Currency Response
@Data
public class AddCurrencyResp {
    //currency info
    private Currency currency;
}
