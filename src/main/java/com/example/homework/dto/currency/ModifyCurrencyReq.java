package com.example.homework.dto.currency;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

// Modify Currency Request
@Data
public class ModifyCurrencyReq {
    //currency name
    @NotBlank
    @Length(min = 1, max = 20)
    private String name;
}
