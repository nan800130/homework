package com.example.homework.dto.currency;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

// Add Currency Request
@Data
public class AddCurrencyReq {
    //currency code
    @NotBlank
    @Length(min = 1, max = 3)
    private String code;

    //currency name
    @NotBlank
    @Length(min = 1, max = 20)
    private String name;
}
