package com.example.homework.dto;

import com.example.homework.enumeration.Result;
import lombok.Data;

@Data
public class Response<T> {
    private String code;
    private String message;
    private T data;

    public Response(){
        this.code = Result.RESULT_SUCCESS.getCode();
        this.message = Result.RESULT_SUCCESS.getDesc();
    }
}
