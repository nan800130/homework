package com.example.homework;

import com.example.homework.model.Currency;
import com.example.homework.service.impl.CurrencyServiceImpl;
import org.junit.Test;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.test.context.junit4.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceTest {

    @MockBean
    private CurrencyServiceImpl currencyServiceImpl;

    @Autowired
    private Currency currency;

    @Test
    public void tetsServiceFindCurrencyByCode() {
        given(this.currencyServiceImpl.findCurrencyByCode("USD"))
                .willReturn(Optional.of(new Currency("USD", "美金")));
        Optional<Currency> usd = this.currencyServiceImpl.findCurrencyByCode("USD");
        assertThat(usd).isEqualTo(Optional.of(new Currency("USD", "美金")));
    }

    @Test
    public void tetsServiceFindAllCurrency() {
        //test for non-empty list
        given(this.currencyServiceImpl.findAllCurrency())
                .willReturn(List.of(new Currency("USD", "美金"), new Currency("GBP", "英鎊")));
        List<Currency> currencyList = List.of(new Currency("USD", "美金"), new Currency("GBP", "英鎊"));
        assertThat(currencyList).isNotEmpty();

        //test for empty list
        given(this.currencyServiceImpl.findAllCurrency())
                .willReturn(new ArrayList<Currency>());
        List<Currency> emptyList = new ArrayList<Currency>();
        assertThat(emptyList).isEmpty();

    }

    @Test
    public void tetsServiceDeleteCurrencyByCode() {

//        List<Currency> emptyList = new ArrayList<Currency>();
//
//        given(this.currencyServiceImpl.findAllCurrency())
//                .willReturn(List.of(new Currency("USD", "美金"), new Currency("GBP", "英鎊")));
//        List<Currency> currencyList = List.of(new Currency("USD", "美金"), new Currency("GBP", "英鎊"));
//        assertThat(currencyList).isNotEmpty();
    }

}
